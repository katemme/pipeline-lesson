/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.pipeline.lesson;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
/**
 *
 * @author kate
 */
public class MainTest {
    
    public MainTest() {
    }

    
    /**
     * Test of addizione method, of class Main.
     */
    @Test
    public void testAddizione() {
        System.out.println("addizione");
        //argomenti di input
        int a = 4;
        int b = 5;
        //valore atteso
        int expResult = 9;
        // in result c'è il valore reale
        int result = Main.addizione(a, b);
        //confronto se il valore atteso è quguale a quello reale
        assertEquals(expResult, result);
    }

    
    /**
     * Test of sottrazione method, of class Main.
     */
    @Test
    public void testSottrazione() {
        System.out.println("sottrazione");
        int a = 6;
        int b = 5;
        int expResult = 1;
        int result = Main.sottrazione(a, b);
        assertEquals(expResult, result);
    }
    
}
